# NeuNet

NeuNet is a custom neural network library developed from scratch in Zig, a low-level programming
language (like C). This library allows users to create and train their own neural networks tailored
to various tasks. The library implements fundamental concepts in machine learning such as gradient
descent and backpropagation algorithms.

With this library, we define the architecture of our neural network, specifying the number of layers
and neurons per layer, and train the model by giving the input and output data sets.

Read under [nn/](./nn/) for more info.

Also see the various projects under [demos/](./demos/) that demonstrate using the library to train
models.

This project was created as an assignment for my college. The original credits goes to [Alexey
Kutepov](https://github.com/rexim) (of the [Tsoding Daily](https://www.youtube.com/@TsodingDaily)
channel) for his [videos](https://www.youtube.com/playlist?list=PLpM-Dvs8t0VZPZKggcql-MmjaBdZKeDMw)
on machine learning.
