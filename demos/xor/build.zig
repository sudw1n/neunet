const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const nn_module = b.addModule("nn", .{
        .root_source_file = .{ .path = "../../nn/src/root.zig" },
        .target = target,
        .optimize = optimize,
        .single_threaded = true,
        .strip = true,
    });

    const exe = b.addExecutable(.{
        .name = "xor",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
        .single_threaded = true,
        .strip = true,
    });

    exe.root_module.addImport("nn", nn_module);

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    run_cmd.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
