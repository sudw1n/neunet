const std = @import("std");
const lib = @import("nn");

const Matrix = lib.Matrix;
const NN = lib.NN;

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    const allocator = arena.allocator();

    var training_data = [_]f64{
        0, 0, 0,
        0, 1, 1,
        1, 0, 1,
        1, 1, 0,
    };

    // stride for the above array
    const stride = 3;
    // rows == number of samples
    const rows = training_data.len / stride;

    // two columns of the above array for input
    const input_cols = 2;
    var training_input = Matrix(f64){
        .rows = rows,
        .cols = input_cols,
        .stride = stride,
        .elements = &training_data,
    };

    const output_cols = 1;
    var training_output = Matrix(f64){
        .rows = rows,
        .cols = output_cols,
        .stride = stride,
        .elements = training_data[2..],
    };

    // A ^ B = (A | B) & ~(A & B)
    // first layer will take A and B
    // first neuron of the hidden layer will represent A | B
    // second neuron of the hidden layer will represent ~(A & B)
    // final output layer will & the two neurons from the hiden layer thus producing the output
    const arch = [_]usize{ 2, 2, 1 };

    var nn = try NN.init(allocator, &arch, 0, 1);
    defer nn.deinit();

    const original_cost = try nn.cost(&training_input, &training_output);

    var gradient = try NN.init(allocator, &arch, 0, 1);
    defer gradient.deinit();

    try stdout.writeAll("training...\n\n");
    try bw.flush();

    const rate = 1e-1;

    const total = 1000 * 1000;
    for (0..total) |i| {
        if (i % 50000 == 0) {
            try stdout.print("Epoch ({d}/{d}): {d}\n", .{ i, total, try nn.cost(&training_input, &training_output) });
            try bw.flush();
        }
        try nn.backPropagate(&gradient, &training_input, &training_output);
        try nn.sub(gradient, rate);
    }
    try stdout.writeByte('\n');

    try stdout.writeAll("successfully trained XOR model\n\n");

    try stdout.print("cost before: {d}\n", .{original_cost});
    try stdout.print("cost after: {d}\n", .{try nn.cost(&training_input, &training_output)});

    try stdout.writeAll("\ntry it out!\n");
    try bw.flush();

    const stdin = std.io.getStdIn();

    var buffer: [20]u8 = undefined;
    var numbers: [2]u8 = undefined;
    var numCount: usize = 0;

    while (true) {
        while (numCount < 2) {
            if (numCount == 0) {
                try stdout.writeAll("a: ");
            } else {
                try stdout.writeAll("b: ");
            }
            try bw.flush();
            if (try stdin.reader().readUntilDelimiterOrEof(&buffer, '\n')) |buf| {
                numbers[numCount] = try std.fmt.parseUnsigned(u8, buf, 10);
                numCount += 1;
            } else {
                std.io.getStdErr().writeAll("Invalid input!") catch unreachable;
                std.process.exit(1);
            }
        }

        var input = nn.getInput();
        var output = nn.getOutput();

        input.at_mut(0, 0).* = @as(f64, @floatFromInt(numbers[0]));
        input.at_mut(0, 1).* = @as(f64, @floatFromInt(numbers[1]));

        nn.forward();

        try stdout.print("model predicted: {d}\n", .{output.at(0, 0)});

        try bw.flush();
        numCount = 0;
    }
}
