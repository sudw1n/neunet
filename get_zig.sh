#!/usr/bin/env bash

set -xe

target=$1
jsonAttr=".master.\"$target\".tarball"
url=$(wget -O - 'https://ziglang.org/download/index.json' 2>/dev/null | jq $jsonAttr | sed 's/"//g')

wget -O - $url 2>/dev/null | tar -xJvf -
rm -rf zig
mv zig-linux-* zig
