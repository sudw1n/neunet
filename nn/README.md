# A Neural Network "framework" written (from scratch) in Zig
This is a simple library that allows creating and training neural networks. The library exposes two
types: `Matrix` (for matrix stuff) and `NN` (for dealing with neural networks).

# Theory
I have explained the various concepts implemented in this library in a separate document. It
involves the mathematical derivations behind the formula used to _gradient_ _descent_, _back
propagation_, etc. The document is named [theory.pdf](./theory/theory.pdf). You can build it
yourself by compiling [theory.tex](./theory/theory.tex).

# How to build
Run `zig build`.

# Documentation
You can get more documentation about the library by running `zig build docs`. The documentation will
be generated (probably; depends on what prefix path you set) at `./zig-out/docs/`. Just open the
`index.html` file in a web browser.

# Examples
The source code at [xor/](../demos/xor/) shows an example usage of this library to model an XOR
gate.
