const std = @import("std");
const testing = std.testing;

// TODO: use the type system to specify the dimensions of the Matrix
//
// maybe accept rows and cols here, so that it's part of the type?
pub fn Matrix(comptime T: type) type {
    return struct {
        /// number of rows for the matrix
        rows: usize,
        /// number of columns for the matrix
        cols: usize,
        /// how many elements to skip in order to get to the next row
        ///
        /// `stride` may not always be equal to `cols`
        stride: usize,
        /// Elements will always be a contiguous array.
        ///
        /// The topology of the matrix is defined by the rows, columns and stride
        elements: []T,

        allocator: std.mem.Allocator = undefined,

        const Self = @This();

        pub const Error = error{InvalidDimensions};

        /// Creates a Matrix of size (m x n)
        pub fn init(allocator: std.mem.Allocator, m: usize, n: usize) !Self {
            return Self{
                .rows = m,
                .cols = n,
                .stride = n,
                .elements = try allocator.alloc(T, m * n),
                .allocator = allocator,
            };
        }

        /// Returns the element of the Matrix at (i, j)
        pub inline fn at(self: Self, i: usize, j: usize) T {
            return self.elements[(i * self.stride) + j];
        }

        /// Returns a pointer to the element of the Matrix at (i, j)
        pub inline fn at_mut(self: *Self, i: usize, j: usize) *T {
            return &self.elements[(i * self.stride) + j];
        }

        /// Fill all of the elements of the matrix with `value`
        pub fn fill(self: *Self, value: T) void {
            for (0..self.rows) |i| {
                for (0..self.cols) |j| {
                    (self.at_mut(i, j)).* = value;
                }
            }
        }

        /// Modify each element of the matrix by applying `op`
        pub fn map(self: *Self, op: *const (fn (T) T)) void {
            for (0..self.rows) |i| {
                for (0..self.cols) |j| {
                    (self.at_mut(i, j)).* = op(self.at(i, j));
                }
            }
        }

        /// Returns the given row of the matrix
        ///
        /// The elements of the returned matrix has the same lifetime as `self`
        /// and doesn't allocate any extra memory.
        ///
        /// It's asserted that `i < self.rows` i.e. the requested row doesn't exceed the maximum
        /// rows the original matrix has.
        pub fn row(self: *Self, i: usize) Matrix(T) {
            std.debug.assert(i < self.rows);
            return Matrix(T){
                .rows = 1,
                .cols = self.cols,
                .stride = self.stride,
                .elements = self.elements[i * self.stride ..],
            };
        }

        /// Copies elements from `src` to `self` in-place.
        ///
        /// Returns `Error.InvalidDimensions` if the sizes of the two matrices aren't equal.
        pub fn copy(self: *Self, src: Matrix(T)) !void {
            if (self.rows != src.rows or self.cols != src.cols) {
                return Error.InvalidDimensions;
            }

            for (0..self.rows) |i| {
                for (0..self.cols) |j| {
                    self.at_mut(i, j).* = src.at(i, j);
                }
            }
        }

        /// Performs matrix multiplication of `self` with `dest`
        /// and updates `dest` with the product.
        ///
        /// Returns `Error.InvalidDimensions` if the two matrices
        /// can't be multiplied because of their dimensions or
        /// because the dimensions of the `dest` matrix isn't
        /// valid.
        pub fn mul(self: Self, other: Self, dest: *Self) Error!void {
            if (self.cols != other.rows or self.rows != dest.rows or other.cols != dest.cols) {
                return Error.InvalidDimensions;
            }

            const n = self.cols;
            for (0..dest.rows) |i| {
                for (0..dest.cols) |j| {
                    dest.at_mut(i, j).* = 0;
                    for (0..n) |k| {
                        dest.at_mut(i, j).* += self.at(i, k) * other.at(k, j);
                    }
                }
            }
        }

        /// Performs matrix addition of `self` with `dest` with each parameter multiplied by an
        /// optional `scalar` parameter, and updates `self` with the sum.
        ///
        /// The `scalar` parameter can be useful if, for example, you want to perform subtraction
        /// (`scalar == -1`) or if you want to multiply each element with a value before performing
        /// the arithmetic.
        ///
        /// Returns `Error.InvalidDimensions` if the two matrices
        /// don't have the same dimensions
        pub fn sum(self: *Self, dest: Self, scalar: ?T) Error!void {
            if (self.rows != dest.rows or self.cols != dest.cols) {
                return Error.InvalidDimensions;
            }
            for (0..self.rows) |i| {
                for (0..self.cols) |j| {
                    var added_value = dest.at(i, j);
                    // branching at each iteration seems kind of redundant but hopefully branch
                    // prediction will take care of it
                    //
                    // i couldn't set a default value for `s` because `T` isn't known
                    // TODO: maybe we could assume `T` can always take the value `1`?
                    if (scalar) |s| {
                        added_value *= s;
                    }
                    (self.at_mut(i, j)).* += added_value;
                }
            }
        }

        pub fn deinit(self: Self) void {
            self.allocator.free(self.elements);
        }

        pub fn format(value: Self, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
            _ = fmt;
            const rows = value.rows;
            const cols = value.cols;

            try writer.writeAll("[\n");

            var row_padding: usize = 2;

            if (options.width) |padding| {
                row_padding += padding;
            }

            for (0..rows) |i| {
                for (0..row_padding) |_| try writer.writeByte(' ');
                for (0..cols) |j| {
                    try writer.print("{d:.8} ", .{value.at(i, j)});
                }
                if (i + 1 != rows) {
                    try writer.writeByte('\n');
                }
            }

            try writer.writeByte('\n');
            for (0..row_padding - 2) |_| try writer.writeByte(' ');
            try writer.writeByte(']');
        }
    };
}

test "detect invalid dimensions" {
    const allocator = testing.allocator;

    // Create matrices with valid dimensions
    const m1 = try Matrix(f32).init(allocator, 2, 3);
    defer m1.deinit();
    const m2 = try Matrix(f32).init(allocator, 3, 4);
    defer m2.deinit();

    // Call dot() with valid dimensions, should not return an error
    var m3 = try Matrix(f32).init(allocator, 2, 4);
    defer m3.deinit();
    try m1.mul(m2, &m3);

    // multiplication with invalid dimensions, should return an error
    var m4 = try Matrix(f32).init(allocator, 4, 3);
    defer m4.deinit();
    try testing.expectError(error.InvalidDimensions, m1.mul(m2, &m4));

    // addition should return error if not identical dimensions
    try testing.expectError(error.InvalidDimensions, m3.sum(m4, null));
}

test "sum" {
    var a_data = [_]i8{
        -1, 4,
        1,  1,
    };
    var a = Matrix(i8){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &a_data,
    };

    var b_data = [_]i8{
        1, 1,
        1, 1,
    };
    const b = Matrix(i8){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &b_data,
    };

    var expected_data = [_]i8{
        0, 5,
        2, 2,
    };

    const expected = Matrix(i8){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &expected_data,
    };

    try a.sum(b, null);

    try testing.expectEqualDeep(a.elements, expected.elements);
}

test "sum with negative scalar is subtraction" {
    var a_data = [_]i8{
        -1, 4,
        1,  1,
    };
    var a = Matrix(i8){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &a_data,
    };

    var b_data = [_]i8{
        1, 1,
        1, 1,
    };
    const b = Matrix(i8){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &b_data,
    };

    var expected_data = [_]i8{
        -2, 3,
        0,  0,
    };

    const expected = Matrix(i8){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &expected_data,
    };

    try a.sum(b, -1);

    try testing.expectEqualDeep(a.elements, expected.elements);
}

test "mul" {
    var m_data = [_]f32{
        1, 0,
        0, 1,
    };

    const m = Matrix(f32){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &m_data,
    };

    var n_data = [_]f32{
        5,
        4,
    };
    const n = Matrix(f32){
        .rows = 2,
        .cols = 1,
        .stride = 1,
        .elements = &n_data,
    };

    var dest = try Matrix(f32).init(testing.allocator, 2, 1);
    defer dest.deinit();

    try m.mul(n, &dest);

    try testing.expectEqualDeep(n.elements, dest.elements);
}

fn test_map_double(x: f32) f32 {
    return x * 2;
}

test "map" {
    var m_data = [_]f32{
        1, 1,
        1, 1,
    };
    var m = Matrix(f32){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &m_data,
    };

    m.map(test_map_double);

    var expected_data = [_]f32{
        2, 2,
        2, 2,
    };

    const expected = Matrix(f32){
        .rows = 2,
        .cols = 2,
        .stride = 2,
        .elements = &expected_data,
    };

    try testing.expectEqualDeep(m.elements, expected.elements);
}
