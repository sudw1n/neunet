const std = @import("std");
const matrix_lib = @import("matrix.zig");
const Matrix = matrix_lib.Matrix;

/// The sigmoid activation function
pub fn sigmoid(x: f64) f64 {
    return 1.0 / (1.0 + @exp(-x));
}

/// Randomize the elements of the matrix within the range [low, high).
///
/// It's assumed low < high.
pub fn randomizeMatrix(matrix: *Matrix(f64), low: f64, high: f64) void {
    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        std.os.getrandom(std.mem.asBytes(&seed)) catch @panic("couldn't get random bytes from OS");
        break :blk seed;
    });
    const rand = prng.random();

    for (0..matrix.rows) |i| {
        for (0..matrix.cols) |j| {
            (matrix.at_mut(i, j)).* = rand.float(f64) * (high - low) + low;
        }
    }
}

/// A neural network defined as a collection of matrices.
pub const NN = struct {
    const MatrixType = Matrix(f64);
    pub const Error = MatrixType.Error || error{ InvalidArchitecture, InvalidDataSet };

    /// How many layers the neural network has.
    ///
    /// Each layer has its own set of weights and biases.
    count: usize,
    /// Weights for neurons in each layer.
    weights: []MatrixType,
    /// Biases for neurons in each layer.
    biases: []MatrixType,
    /// Amount of activations is count + 1.
    ///
    /// activations[0] is the input layer.
    activations: []MatrixType,

    allocator: std.mem.Allocator,

    /// Initialize a new neural network with the given architecture.
    ///
    ///
    /// `arch` defines the architecture of the neural network.
    ///
    /// For example, `arch == {2, 2, 1}` means the first layer has two neurons, the second (hidden)
    /// layer also has two neurons and the final (output) layer is a single neuron.
    ///
    /// `low` and `high` define the range of the values for the parameters.
    ///
    /// Returns `Error.InvalidArchitecture` if `arch.len` == 0.
    pub fn init(allocator: std.mem.Allocator, arch: []const usize, low: f64, high: f64) !NN {
        if (arch.len == 0) {
            return Error.InvalidArchitecture;
        }

        // `arch.len` also includes the input layer so the actual count of the neuron is `arch.len - 1`
        const count = arch.len - 1;
        var nn = NN{
            .count = count,
            .allocator = allocator,
            .weights = try allocator.alloc(MatrixType, count),
            .biases = try allocator.alloc(MatrixType, count),
            // activations includes the input layer
            .activations = try allocator.alloc(MatrixType, count + 1),
        };

        // the input matrix
        nn.activations[0] = try MatrixType.init(allocator, 1, arch[0]);
        for (1..arch.len) |i| {
            // we will perform matrix multiplication of the weights with the activations
            // so the dimensions are defined accordingly
            nn.weights[i - 1] = try MatrixType.init(allocator, nn.activations[i - 1].cols, arch[i]);
            randomizeMatrix(&nn.weights[i - 1], low, high);
            // dimension should be row from activation and column from weight since they will be
            // multiplied and the result is added to bias
            nn.biases[i - 1] = try MatrixType.init(allocator, 1, arch[i]);
            randomizeMatrix(&nn.biases[i - 1], low, high);
            // next activation
            nn.activations[i] = try MatrixType.init(allocator, 1, arch[i]);
        }

        return nn;
    }

    /// Fill all of the values of the given neural network with zero.
    pub fn fill_zero(self: *NN) void {
        for (0..self.count) |i| {
            self.weights[i].fill(0.0);
            self.biases[i].fill(0.0);
            self.activations[i].fill(0.0);
        }
        // we have count + 1 activations
        self.activations[self.count].fill(0.0);
    }

    /// Get the input matrix of the neural network
    pub inline fn getInput(self: *NN) *MatrixType {
        return &self.activations[0];
    }

    /// Get the output matrix of the neural network
    pub inline fn getOutput(self: *NN) MatrixType {
        return self.activations[self.count];
    }

    /// Check if the architecture of `self` and `other` are same.
    pub inline fn checkDimensions(self: NN, other: NN) Error!void {
        // in `init()`, we use `arch.len` to determine the count and that count determines other
        // dimensions
        if (self.count != other.count) return Error.InvalidDimensions;
    }

    /// Forward the inputs through the neural network (forward pass).
    ///
    /// The output will be available in the final (output) neuron(s).
    ///
    /// The caller has to ensure that the inputs are placed at the first activation layer,
    /// i.e. `self.activations[0]` (see `getInput()`).
    ///
    /// The output will subsequently be available in the final element of `self.activations` (see `getOutut()`).
    pub fn forward(self: *NN) void {
        // modeled after the formula in Section 2.4.1 of theory.pdf
        for (0..self.count) |l| {
            // we must have ensured that the dimenstions are set properly so the caller doesn't
            // have to worry about it
            self.activations[l].mul(self.weights[l], &self.activations[l + 1]) catch unreachable;
            // we have ensured that `self.activations.len` == self.count+1,
            // therefore this will not overflow
            self.activations[l + 1].sum(self.biases[l], null) catch unreachable;
            self.activations[l + 1].map(sigmoid);
        }
    }

    /// Determines the cost of the neural network.
    ///
    /// The cost is essentially the error between the predicted values (calculated by forwarding
    /// inputs given in `train_input`) to the real values (given in `train_output`)
    ///
    /// Returns `Error.InvalidDimensions` if:
    /// - `train_input.rows != train_output.rows` (i.e. the number of samples in the input and the output should be the same)
    /// - `train_output.cols != self.getOutput().cols`
    /// - `train_input.cols != self.getInput().cols`
    // TODO: take train_input and train_output in init() so that we can do the respective checks there
    pub fn cost(self: *NN, train_input: *Matrix(f64), train_output: *Matrix(f64)) Error!f64 {
        if (train_input.rows != train_output.rows or train_output.cols != self.getOutput().cols) {
            return Error.InvalidDimensions;
        }
        // number of samples
        const n = train_input.rows;

        var c: f64 = 0;
        for (0..n) |i| {
            const x = train_input.row(i);
            // expected values for this particular sample
            const expected = train_output.row(i);

            try self.getInput().copy(x);
            self.forward();

            const y = self.getOutput();

            // iterate through all of the outputs and determine the cost
            for (0..train_output.cols) |j| {
                const d = y.at(0, j) - expected.at(0, j);
                c += d * d;
            }
        }

        return c / @as(f64, @floatFromInt(n));
    }

    /// Calculate finite difference of the cost resulting from wiggling the parameters with `eps`.
    ///
    /// Places the resulting difference in the corresponding parameter of `gradient`.
    ///
    /// The `train_input` and `train_output` are for calculating the cost. See `cost`.
    pub fn finiteDifference(self: *NN, gradient: *NN, train_input: *MatrixType, train_output: *MatrixType, eps: f64) Error!void {
        const original_cost = try self.cost(train_input, train_output);
        var saved: f64 = 0;

        for (0..self.count) |i| {
            var weight = self.weights[i];
            for (0..weight.rows) |j| {
                for (0..weight.cols) |k| {
                    saved = weight.at(j, k);
                    weight.at_mut(j, k).* += eps;
                    // don't need to check if cost() returns an error since we have done it above
                    // already
                    const new_cost = self.cost(train_input, train_output) catch unreachable;
                    gradient.weights[i].at_mut(j, k).* = (new_cost - original_cost) / eps;
                    weight.at_mut(j, k).* = saved;
                }
            }

            var bias = self.biases[i];
            for (0..bias.rows) |j| {
                for (0..bias.cols) |k| {
                    saved = bias.at(j, k);
                    bias.at_mut(j, k).* += eps;
                    // don't need to check if cost() returns an error since we have done it above
                    // already
                    const new_cost = self.cost(train_input, train_output) catch unreachable;
                    gradient.biases[i].at_mut(j, k).* = (new_cost - original_cost) / eps;
                    bias.at_mut(j, k).* = saved;
                }
            }
        }
    }

    /// Performs backpropagation from the given input set (`train_input`) and output set
    /// (`train_output`). Updates parameters of `self` in-place.
    ///
    /// Will store the gradient values in `gradient`.
    ///
    /// Returns `Error.InvalidDataSet` if the number of samples (rows) from the input and that of
    /// the output isn't equal or if the number of outputs (columns in the output of the neural
    /// network) isn't equal to that of `train_output`.
    ///
    /// Return `Error.InvalidArchitecture` if the architecture of `gradient` isn't the same as
    /// `self`.
    pub fn backPropagate(self: *NN, gradient: *NN, train_input: *MatrixType, train_output: *MatrixType) !void {
        try self.checkDimensions(gradient.*);

        var output = self.getOutput();
        var gradient_output = gradient.getOutput();

        if (train_input.rows != train_output.rows or output.cols != train_output.cols) {
            // number of samples from the input should be same as that of the output
            return Error.InvalidDataSet;
        }

        const n = train_input.rows;

        // fill all of the values in the gradient with zero
        gradient.fill_zero();

        // i -> current sample
        // l -> current layer
        // j -> current activation
        // k -> previous activation
        for (0..n) |i| {

            // feed-forward
            self.getInput().copy(train_input.row(i)) catch unreachable;
            self.forward();

            for (0..self.count) |j| {
                gradient.activations[j].fill(0.0);
            }

            // calculate the gradient of the output (activation of the final layer)
            for (0..output.cols) |j| {
                gradient_output.at_mut(0, j).* = output.at(0, j) - train_output.at(i, j);
            }

            //   for each layers:
            //     for each activation in that layer:
            //       for each previous activation connected to current activation:
            // refer to Section 2.4.2 in theory.pdf for the formulas
            // partial derivative values reside in `gradient` and the non-partial values reside in
            // `self`
            var l = self.count;
            while (l > 0) : (l -= 1) {
                for (0..self.activations[l].cols) |j| {
                    const a = self.activations[l].at(0, j);
                    const da = gradient.activations[l].at(0, j);
                    // for bias, we don't need activations of previous layer
                    const temp = 2 * da * a * (1 - a);
                    gradient.biases[l - 1].at_mut(0, j).* += temp;
                    for (0..self.activations[l - 1].cols) |k| {
                        //                 [ w_{l-1} ... ]
                        // [a_{l-1} ... ]  [ a_{l} ... ]
                        // j -> weight matrix column
                        // k -> weight matrix row

                        // one of the previous activations
                        const pa = self.activations[l - 1].at(0, k);

                        gradient.weights[l - 1].at_mut(k, j).* += temp * pa;
                        gradient.activations[l - 1].at_mut(0, k).* += temp * self.weights[l - 1].at(k, j);
                    }
                }
            }
        }

        // we calculated the sum, now calculate the average of the sums
        for (0..gradient.count) |i| {
            for (0..gradient.weights[i].rows) |j| {
                for (0..gradient.weights[i].cols) |k| {
                    gradient.weights[i].at_mut(j, k).* /= @as(f64, @floatFromInt(n));
                }
            }
            for (0..gradient.biases[i].rows) |j| {
                for (0..gradient.biases[i].cols) |k| {
                    gradient.biases[i].at_mut(j, k).* /= @as(f64, @floatFromInt(n));
                }
            }
        }
    }

    /// Subtract values of `other` from `self` multiplied by `rate`.
    ///
    /// This is basically a wrapper to `Matrix(T).sum()`.
    ///
    /// Returns `Error` if `self` and `other` are incompatible for the subtraction.
    pub fn sub(self: *NN, other: NN, rate: f64) Error!void {
        try self.checkDimensions(other);

        for (0..self.count) |i| {
            var self_weight = self.weights[i];
            const other_weight = other.weights[i];

            try self_weight.sum(other_weight, -rate);

            var self_bias = self.biases[i];
            const other_bias = other.biases[i];

            try self_bias.sum(other_bias, -rate);
        }
    }

    pub fn deinit(self: NN) void {
        for (self.weights) |weight| {
            weight.deinit();
        }
        self.allocator.free(self.weights);
        for (self.biases) |bias| {
            bias.deinit();
        }
        self.allocator.free(self.biases);
        for (self.activations) |activation| {
            activation.deinit();
        }
        self.allocator.free(self.activations);
    }

    pub fn format(self: NN, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        try writer.writeAll("[\n");
        for (0..self.count) |i| {
            try writer.print("  weights[{d}] = ", .{i});
            try self.weights[i].format(fmt, options, writer);
            try writer.writeByte('\n');
            try writer.print("  biases[{d}] = ", .{i});
            try self.biases[i].format(fmt, options, writer);
            try writer.writeByte('\n');
        }
        try writer.writeAll("]\n");
    }
};
