const std = @import("std");

const matrix_lib = @import("matrix.zig");
const nn_lib = @import("nn.zig");

pub const Matrix = matrix_lib.Matrix;
pub const randomizeMatrix = nn_lib.randomizeMatrix;
pub const sigmoid = nn_lib.sigmoid;
pub const NN = nn_lib.NN;

comptime {
    std.testing.refAllDecls(@This());
}
