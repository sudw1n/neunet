\documentclass[a4paper, 12pt]{article}
\usepackage{mathtools}
\usepackage[%
    math-style=ISO,
    warnings-off={mathtools-colon, mathtools-overbracket},
]{unicode-math}
\usepackage{csquotes}
\usepackage{tikz}
% this needs to be the last package loaded
\usepackage[%
    colorlinks, pdfusetitle, hyperfootnotes, hyperfigures
]{hyperref}

\hypersetup{
    urlcolor = black,
    citecolor = red,
    linkcolor = black,
    pdftitle={AI Mini Project}
}

\author{Pratik P. Devkota}
\title{AI Mini Project}

\NewDocumentCommand{\avgsum}{mm}{\frac{1}{#2}\sum_{#1=1}^{#2}}
% PARtial DERivative
\NewDocumentCommand{\parder}{m}{\partial_{#1}}

\begin{document}
\section{Introduction}
This document describes the theory behind my mini project.

Training every machine learning model begins with defining the input and output data set. We then
define our model in terms of a simple regression formula. Suppose, $y$ is the expected output from
our model and $x$ is the given input, then we define an equation:
\[
    y = xw + b
\]
Here, $w$ is a single parameter for our model and $b$ is the bias. \enquote{Training our model}
basically means tuning the parameters so that $y$ is as close to the expected output as possible.

In order to quantify how well the model is performing, we use a \emph{cost function} that measures
the mismatch between the predicted outcomes and the true outcomes. For regression problems, we often
use a \textit{Mean Squared Error} (MSE) as the cost function:
\begin{equation}
    C(w) = \avgsum{i}{n} \left( y_i - \hat{y}_i \right)^2\label{eq:cost}
\end{equation}

Here, $y_i$ is the expected output, $\hat{y}_i$ is the predicted output and $w$ is the parameter for
the model. We square the difference as it gives more weight to larger errors. This is desirable
because it penalizes larger errors more heavily, making the optimization process more sensitive to
improving predictions for extreme cases. Additionally, we also eliminate the need to deal with
positive and negative errors. Finally, we compute sum of the squares for all samples, $n$, then
divide the sum by the total numbers of samples.

\subsection{Gradient Descent}
In multivariable calculus, the gradient is a vector containing the partial derivatives of the
function with respect to each variable. If we have multiple parameters, say
$w_1, w_2, \ldots, w_k$, the gradient would be represented as:
\[
    \nabla C = \left( \frac{\partial C}{\partial w_1}, \frac{\partial C}{\partial w_2}, \ldots, \frac{\partial C}{\partial w_k} \right)
\]

The partial derivatives measure how the cost function changes concerning each individual parameter
while holding the other parameters constant. In practical terms, the gradient points in the
direction of the steepest increase of the cost function. By adjusting the parameters in the opposite
direction of the gradient, we aim to minimize the cost function during the training process.

Gradient descent is an algorithm used to optimize a machine learning model by iteratively moving
towards the local minima of the cost function. The main idea is as follows:
\begin{itemize}
    \item Calculate the gradient (partial derivative) of the cost function with respect to each
        parameter.
    \item Adjust the parameters in the opposite direction of the gradient to minimize the cost
        function. Suppose $C$ is the cost function, and $\theta$ is one of its paramaters. Then we
        update our parameters using the gradient with the following rule:
        \[
            \theta = \theta - \alpha \cdot \nabla C
        \]
        Here, $\alpha$ is a hyperparameter\footnote{a parameter external to the model than cannot be
        learned from the training data} that controls how much we are adjusting the weights of our
        model with respect to the cost gradient.
\end{itemize}

\section{Models}
In this section, I describe the various models I have developed for this project.

To find the derivative of the cost functions for each of the models, we could use the formula:
\begin{equation}
    C'(w) = \lim_{\epsilon \to 0}\frac{C(w+\epsilon) - C(w)}{\epsilon}
    \label{eq:suboptimal-derivative}
\end{equation}

However, this approach isn't very optimal as it requires executing the cost function twice. With
large samples, the cost function may introduce a significant amount of overhead. Therefore, we
derive the gradient mathematically then use the final derivation in our programs instead of
Equation~\ref{eq:suboptimal-derivative}.

\subsection{The \enquote{Twice} Model}\label{section:twice-model}
As a starting point, I have modeled a neuron that is supposed to double its input. The source code
lives at \verb|twice/twice.zig|. This describes a simple linear model with a single parameter: $w$.

\begin{center}
    \begin{tikzpicture}
        \node(X) at (-1.5, 0) {$x$};
        \node[draw, circle](N) at (0, 0) {$w$};
        \node(Y) at (1.5, 0) {$y$};
        \draw[arrows=->] (X) -- (N);
        \draw[arrows=->] (N) -- (Y);
    \end{tikzpicture}
\end{center}

The equation for the model is a simple linear formula:
\[
    y = x \cdot w
\]

Then, Equation~\ref{eq:cost} becomes:

\begin{align}
    C(w) = \avgsum{i}{n} \left( x_iw - y_i \right)^2
\end{align}

Here, $x_i$ and $y_i$ are the input and output for the $i$th sample respectively.

Derivating with respect to $w$,

\begin{align*}
    C'(w)
     &= \left(\avgsum{i}{n} \left( x_iw - y_i \right)^2\right)'
        && \text{definition} \\
     &= \avgsum{i}{n} \left( \left( x_iw - y_i \right)^2\right)' && \text{constants unchanged} \\
     &= \avgsum{i}{n} 2\left( x_iw - y_i \right)x_i && \text{chain rule}
\end{align*}

Therefore, we can use this instead of Equation~\ref{eq:suboptimal-derivative} in order to calculate
the gradient. Using this formula also gives better accuracy because we have eliminated the need to
have an $\epsilon$ value as well as achieved better performance due to reduced computation.

For the single-variable case, the derivative $C'(w)$ was sufficient for optimization. However, with
multiple variables, we now consider the entire gradient vector $\nabla C$ to update all parameters
simultaneously.

\subsection{One Neuron Model with 2 inputs}

The \enquote{twice} model can be generalized to a model with a single neuron. We simplified it by
not including a bias term ($b$). However, in a more general single-neuron model, a bias term is
crucial as it allows the neuron to have an influence even when the input is zero.

\begin{center}
    \begin{tikzpicture}
        \node(X) at (-2, 1.5) {$x$};
        \node(Y) at (-2, -1.5) {$y$};
        \node[draw, circle](N) at (0, 0) {$\sigma,b$};
        \node(A1) at (2, 0) {$a_1$};
        \draw[arrows=->] (X) -- node[above] {$w_1$} (N);
        \draw[arrows=->] (Y) -- node[above] {$w_2$} (N);
        \draw[arrows=->] (N) -- (A1);
    \end{tikzpicture}
\end{center}

Additionally, for complex networks, defining the entire network as a single linear transformation
would severely restrict the network's ability to learn complex relationships in the data. In order
to introduce non-linearity in the models, we use an \emph{activation function}.

For the previous model, we used $y$ to denote the output of a model. Since that model had a single
neuron, the output of that neuron was itself the output of the entire model. However, as we start
dealing with models with multiple neurons, we will need a better notation for such outputs. To that
regard, let's call the output of a neuron the \enquote{activation of the neuron} denoting it as
$a_i$ - the activation for the $i$th sample. Additionally, we now have two inputs, $x$ and
$y$ defined\footnote{note that $y$ in this case is used to denote the input}, so the equation of the
model becomes:

\begin{equation}
    a_i = \sigma(x_iw_1 + y_iw_2 + b) \label{eq:activation-single-neuron}
\end{equation}

Here, $\sigma$ is a commonly used activation function called the \emph{sigmoid function}. It
squashes its input into the range $(0, 1)$, effectively converting the weighted sum into a
probability-like output. This is particularly useful in binary classification problems, where the
neuron's output can be interpreted as the probability of belonging to a particular class. For models
with multiple neurons, this can also be used to determine if a particular neuron should be active or
not. Mathematically, the sigmoid function is defined as:

\begin{align*}
    \sigma(x)
    &= \frac{1}{1 + e^{-x}}
\end{align*}

Its derivative is defined as:
\begin{align*}
    \sigma'(x)
    &= \sigma(x)(1 - \sigma(x))
\end{align*}

Since we're going to need them later, let's derive partial derivatives of
Equation~\ref{eq:activation-single-neuron} with respect to each of its parameters.

\begin{align*}
    \parder{w_1}a_i
    &= a_i (1 - a_i) \parder{w_1}(x_iw_1 + y_iw_2 + b) && \text{} \\
    &= a_i (1 - a_i) x_i && \text{} \\
    \parder{w_2}a_i
    &= a_i (1 - a_i) \parder{w_2}(x_iw_1 + y_iw_2 + b) && \text{} \\
    &= a_i (1 - a_i) y_i && \text{} \\
    \parder{b}a_i
    &= a_i (1 - a_i) \parder{b}(x_iw_1 + y_iw_2 + b) && \text{}
    &= a_i (1 - a_i) && \text{} \\
\end{align*}

Similar to what we did in Section~\ref{section:twice-model}, Equation~\ref{eq:cost} becomes:

\begin{align*}
    C
    &= \avgsum{i}{n} \left( a_i - z_i \right)^2\label{eq:cost-one-neuron}
\end{align*}

Here, I use $C$ to denote the cost function, and $z$ is the expected output for the neuron.

In this case, since we have three different parameters - $w_1$, $w_2$ and $b$ - we need to find the partial
derivative with respect to each of the parameters.

Now, differentiating the cost function partially with respect to $w_1$:

\begin{align*}
    \parder{w_1}C
     &= \avgsum{i}{n} \parder{w_1} \left( \left( a_i - z_i \right)^2 \right) && \text{definition} \\
     &= \avgsum{i}{n} 2 \left( a_i - z_i \right) \parder{w_1}a_i && \text{chain rule} \\
     &= \avgsum{i}{n} 2 \left( a_i - z_i \right) a_i (1 - a_i) x_i && \text{}
\end{align*}

Now, with respect to $w_2$:

\begin{align*}
    \parder{w_2}C
     &= \avgsum{i}{n} 2 \left( a_i - z_i \right) a_i (1 - a_i) y_i && \text{}
\end{align*}

Similarly, with respect to $b$:
\begin{align*}
    \parder{b}C
     &= \avgsum{i}{n} 2 \left( a_i - z_i \right) a_i (1 - a_i) && \text{}
\end{align*}

\subsection{Two Neuron Model with 1 input}\label{sect:two-neuron-model}

Now we're starting to deal with more neurons. In this model, each number in the superscript of the
symbols represents the layer to which those symbols belong to. So, the input (what we previously
called $x$) can be thought of as the activation of the $0$th layer and the output (what we
previously called $y$) is the activation of the second layer \textit{i.e.}, $a^{(2)}$. The input for
the final layer will come from the output of the first layer \textit{i.e.}, $a^{(1)}$ will be the
input for $a^{(2)}$. This is known as \textbf{forward pass}.

\pagebreak

\begin{center}
    \begin{tikzpicture}
        \node(A0) at (-2.5, 0) {$a^{(0)}$};
        \node[draw, circle](N1) at (0, 0) {$\sigma,b^{(1)}$};
        \node[draw, circle](N2) at (2.5, 0) {$\sigma,b^{(2)}$};
        \node(A2) at (5, 0) {$a^{(2)}$};
        \draw[arrows=->] (A0) -- node[above] {$w^{(1)}$} (N1);
        \draw[arrows=->] (N1) -- node[above] {$w^{(2)}$} (N2);
        \draw[arrows=->] (N2) -- (A2);
    \end{tikzpicture}
\end{center}

If $i$ be the current sample, then the activations of each layer is defined as:

\begin{align*}
    a_{i}^{(1)}
    &= \sigma(a_{i}^{(0)}w^{(1)} + b^{(1)}) \\
    a_{i}^{(2)}
    &= \sigma(a_{i}^{(1)}w^{(2)} + b^{(2)})
\end{align*}


As before, we will also define the partial derivatives:
\begin{align*}
    \parder{w^{(1)}} a_{i}^{(1)}
    &= a_{i}^{(1)} (1 - a_{i}^{(1)}) a_{i}^{(0)} \\
    \parder{b^{(1)}} a_{i}^{(1)}
    &= a_{i}^{(1)} (1 - a_{i}^{(1)}) \\
    \parder{w^{(2)}} a_{i}^{(2)}
    &= a_{i}^{(2)} (1 - a_{i}^{(2)}) a_{i}^{(1)} \\
    \parder{b^{(2)}} a_{i}^{(2)}
    &= a_{i}^{(2)} (1 - a_{i}^{(2)})
\end{align*}

For $a_{i}^{(2)}$, $a_{i}^{(1)}$ is also a parameter:
\begin{align*}
    \parder{a_{i}^{(1)}} a_{i}^{(2)}
    &= a_{i}^{(2)} (1 - a_{i}^{(2)}) w_{i}^{(2)}
\end{align*}

Now if $z_{i}$ be the expected output of the current sample, then the cost function of the second
layer will be:

\begin{align*}
    C^{(2)}
    &= \avgsum{i}{n} \left( a_{i}^{(2)} - z_{i} \right)^{2}
\end{align*}

Let's find the partial derivatives with respect to parameters of the second neuron.
\begin{align*}
    \parder{w^{(2)}}C^{(2)}
    &= \avgsum{i}{n} 2 \left( a_{i}^{(2)} - z_{i} \right) \parder{w^{(2)}} a_{i}^{(2)} \\
    &= \avgsum{i}{n} 2 \left( a_{i}^{(2)} - z_{i} \right) a_{i}^{(2)} (1 - a_{i}^{(2)}) a_{i}^{(1)} \\
    \parder{b^{(2)}}C^{(2)}
    &= \avgsum{i}{n} 2 \left( a_{i}^{(2)} - z_{i} \right) \parder{b^{(2)}} a_{i}^{(2)} \\
    &= \avgsum{i}{n} 2 \left( a_{i}^{(2)} - z_{i} \right) a_{i}^{(2)} (1 - a_{i}^{(2)}) \\
    \parder{a_{i}^{(1)}}C^{(2)}
    &= \avgsum{i}{n} 2 \left( a_{i}^{(2)} - z_{i} \right) \parder{a_{i}^{(1)}} a_{i}^{(2)} \\
    &= \avgsum{i}{n} 2 \left( a_{i}^{(2)} - z_{i} \right) a_{i}^{(2)} (1 - a_{i}^{(2)}) w_{i}^{(2)}
\end{align*}

The equation given by $\parder{a_{i}^{(1)}}C^{(2)}$ also represents the difference between the
predicted value and the expected value of $a^{(1)}$. In other words, if $e_{i}$ be the expected
value, then:
\begin{align*}
    \parder{a_i^{(1)}}C^{(2)}
    &= a_i^{(1)} - e_i \\
    e_i
    &= a_i^{(1)} - \parder{a_i^{(1)}}C^{(2)} \\
\end{align*}

The cost function of the first layer will be:
\begin{align*}
    C^{(1)}
    &= \avgsum{i}{n} \left( a_{i}^{(1)} - e_{i} \right)^{2} \\
\end{align*}

Derivating $C^{(1)}$ with respect to the (variable) parameters of its layer:

\begin{align*}
    \parder{w^{(1)}}C^{(1)}
    &= \avgsum{i}{n} 2 ( \parder{a_i^{(1)}}C^{(2)} ) a_{i}^{(1)} (1 - a_{i}^{(1)}) a_{i}^{(0)} \\
    \parder{b^{(1)}}C^{(1)}
    &= \avgsum{i}{n} 2 ( \parder{a_i^{(1)}}C^{(2)} ) a_{i}^{(1)} (1 - a_{i}^{(1)}) \\
\end{align*}

We calculate the gradients of the loss function with respect to each weight and bias, starting from
the second layer and working our way backwards. This tells us how much each parameter contributes to
the overall error and in which direction we should adjust them to minimize it. This technique is
known as \textbf{backpropagation}.

\subsection{Arbitrary Neurons Model with 1 input}

We can finally generalize these concepts to deal with an arbitrary number of neurons, say, $m$.
We'll do this in two sections. First, we basically define the equations starting from the input
layer (\textit{feed-forward}). Then when we're computing the cost function, we do it
starting from the output layer (\textit{backpropagation}).

\subsubsection{Feed-Forward}
Let $l$ be the current layer and $i$ be the current sample, then:
\begin{align*}
    a_{i}^{(l)}
    &= \sigma(a_{i}^{(l-1)}w^{(l)} + b^{(l)}) \\
    \parder{w^{(l)}} a_{i}^{(l)}
    &= a_{i}^{(l)} (1 - a_{i}^{(l)}) a_{i}^{(l-1)} \\
    \parder{b^{(l)}} a_{i}^{(l)}
    &= a_{i}^{(l)} (1 - a_{i}^{(l)}) \\
    \parder{a_{i}^{(l-1)}} a_{i}^{(l)}
    &= a_{i}^{(l)} (1 - a_{i}^{(l)}) w_{i}^{(l)}
\end{align*}

\subsubsection{Backpropagation}

In Section~\ref{sect:two-neuron-model}, we found that the difference between the predicted output
and expected output can be calculated by the partial derivative of the cost function from the next
layer. In other words, if $a_{i}^{(l)}$ be the activation of the current layer, and $e_i^{(l)}$ be
the expected value, then we can denote $a_{i}^{(l)} - e_i^{(l)}$ as $\parder{a_i^{(l)}} C^{(l+1)}$.

\begin{align*}
    C^{(l)}
    &= \avgsum{i}{n} ( \parder{a_i^{(l)}} C^{(l+1)} )^2 \\
    \parder{w^{(l)}}C^{(l)}
    &= \avgsum{i}{n} 2 ( \parder{a_i^{(l)}}C^{(l+1)} ) a_{i}^{(l)} (1 - a_{i}^{(l)}) a_{i}^{(l-1)} \\
    \parder{b^{(l)}}C^{(l)}
    &= \avgsum{i}{n} 2 ( \parder{a_i^{(l)}}C^{(l+1)} ) a_{i}^{(l)} (1 - a_{i}^{(l)}) \\
    \parder{a_{i}^{(l-1)}}C^{(l)}
    &= \avgsum{i}{n} 2 ( \parder{a_i^{(l)}}C^{(l+1)} ) a_{i}^{(l)} (1 - a_{i}^{(l)}) w_{i}^{(l)} \\
\end{align*}

\end{document}
