const std = @import("std");

const training_data = [_][2]f64{
    .{ 0, 0 },
    .{ 1, 2 },
    .{ 2, 4 },
    .{ 3, 6 },
    .{ 4, 8 },
    .{ 5, 10 },
    .{ 6, 12 },
    .{ 7, 14 },
    .{ 8, 16 },
    .{ 9, 18 },
    .{ 10, 20 },
};

// the cost function
fn cost(w: f64) f64 {
    var result: f64 = 0;
    const n = training_data.len;
    for (0..n) |i| {
        const x = training_data[i][0];
        const expected = training_data[i][1];
        const predicted = x * w;
        const d = expected - predicted;
        result += d * d;
    }
    return result / n;
}

// the derivative of the cost function
fn dcost(w: f64) f64 {
    var result: f64 = 0;
    const n = training_data.len;
    for (0..n) |i| {
        const x = training_data[i][0];
        const expected = training_data[i][1];
        result += 2 * (x * w - expected) * x;
    }
    return result / n;
}

pub fn main() !void {
    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        std.os.getrandom(std.mem.asBytes(&seed)) catch @panic("couldn't get random bytes from OS");
        break :blk seed;
    });
    const rand = prng.random();

    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    var w = rand.float(f64);
    const rate = 1e-3;

    try stdout.print("initial cost: {d}, w = {d}\n", .{ cost(w), w });

    try stdout.writeByte('\n');

    try stdout.writeAll("initial results\n");
    for (training_data) |data| {
        try stdout.print("x: {d}, y: {d}, predicted_y: {d}\n", .{ data[0], data[1], data[0] * w });
    }
    try stdout.writeByte('\n');

    try stdout.writeAll("training");
    try bw.flush();

    for (0..1000) |i| {
        if (i % 200 == 0) {
            try stdout.writeByte('.');
            try bw.flush();
        }
        const gradient_w = dcost(w);
        w -= rate * gradient_w;
    }

    try stdout.writeByte('\n');
    try stdout.print("final cost: {d}, w = {d}\n", .{ cost(w), w });
    try stdout.writeByte('\n');

    try stdout.writeAll("final results\n");
    for (training_data) |data| {
        try stdout.print("x: {d}, y: {d}, predicted_y: {d}\n", .{ data[0], data[1], data[0] * w });
    }

    try bw.flush();
}
