---
marp: true
theme: uncover
paginate: true
---

<!-- so for me, AI and Machine Learning was very much an unexplored territory. So with this project
I wanted to take it as an opportunity to learn from the ground up. As a proud believer in "learning
from the first principles", I wanted to do everything from scratch. So with the amount of time and
effort I could utilize, I've ended up with what I am now calling "NeuNet". Of course, it would be a
shame if I didn't use AI to generate this name. -->
<!-- NeuNet is a "framework" that is written from scratch in Zig -->
<!-- which is a low-level language similar to C -->
# NeuNet
* A neural network "framework" developed in Zig

---

# How it works
<!-- how we use it is we first define the architecture of our neural network -->
<!-- then we supply our own input and output data sets -->
<!-- finally we use the trained model to predict data  -->
* Define the architecture of your neural network
* Train by supplying your own data sets
* use trained model to predict test data

---
<!-- So let me go over some of the concepts involved  -->
<!-- we define every model as some sort of a linear equation  -->
<!-- here x is the input, w and b are the parameters -->
<!-- training the model basically means tuning these parameters so that we get the closest
representation of the model -->
<!-- the cost function quantifies how inaccurate our model is -->
# Model and Cost function
* model equation: $y = xw + b$
* cost function: quantify how inaccurate the model is
---
<!-- so our objective is to minimize the cost function -->
<!-- suppose our cost function is this parabola -->
<!-- then we need to find the local minimum -->
<!-- I say local minimum because there isn't an computationally inexpensive way find the global
minima of a function -->
* need to minimize cost function
* ![w:500](./images/cost-function.png)
---
<!-- to do this we utilize a technique called gradient descent -->
<!-- so we know that the gradient of a function gives the direction of its steepest increase -->
<!-- in gradient descent, we move the parameters in the opposite direction of the gradient -->
<!-- so in this figure, the gradient points upwards so we wiggle our parameters in order to move
downwards towards the minima -->
* minimize cost using gradient descent
* gradient: steepest increase
* gradient descent: move parameters opposite to gradient
* ![w:500](./images/gradient-descent.png)
---
# Back Propagation
<!-- but what if we have a lot of neurons -->
<!-- in that case, finding the gradient is a lot difficult -->
<!-- that is where the concept of back propagation comes in -->
<!-- suppose we have a two layer network with one neuron in each layer -->
<!-- in order to apply back propagation, we first forward the inputs to the network
so that we get some result at the output layer -->
<!-- then we use that result to calculate the gradient of the neurons at the final layer -->
<!-- then we use those gradients to calculate gradients of the previous layers -->
<!-- so we recursively find the gradients updating the parameters at each layer until we reach the
first layer -->
<!-- now there's a lot more to these concepts than I've mentioned here.
Implementing them requires us to first mathematically derive various formulas and then use those
formulas to come up with an algorithm which turned out to be a complex task. -->
<!-- Anyway, in my project repository you can find a document called theory.pdf that goes over these
concepts in much details and also shows how we can derive the various formulas involved. -->
* ![w:600](./images/neurons.png)
* forward inputs to get result at output layer
* calculate gradients of output layer
* use those gradients to calculate gradients of previous layer
* repeat until reach first layer
* please see [theory.pdf](./nn/theory/theory.pdf) for more details

---

# Forwarding Inputs
<!-- so at the end of the document, we are left with the following formulas  -->
<!-- this one is used for forwarding the inputs -->
final formula:
![w:1000](./images/feed-forward-formula.png)

---
# Back Propagation
<!-- this one is used for back propagation -->
![w:1000](./images/back-propagation.png)

---
# Demos
<!-- so let's see a demo -->
---
<!-- we can use this library to model an XOR gate -->
<!-- for some reason, we apparently can't use a single neuron to model XOR -->
<!-- but we can break it down, right? -->
<!-- we can represent XOR as a combination of OR, NAND, and AND gates -->
<!-- so the architecture of our model will consist of three layers -->
<!-- the first layer is basically for inputs -->
<!-- the second layer represents the OR and NAND gates -->
<!-- and the final layer represents the AND gate -->
<!-- and of course, the training data set will be the truth table of XOR -->
<!-- so let's look at the code -->
# Modeling an XOR gate
* XOR couldn't be modeled with a single neuron
* `A ^ B = (A | B) & ~(A & B)`
* `arch = {2, 2, 1}`
* first layer == `A` and `B` (inputs)
* second layer ~= `A | B` and `~(A & B)`
* final layer ~= `A & B`
* training: truth table of XOR
---
<!-- we can scale this to a model that recognizes handwritten digits -->
<!-- we can use the MNIST database of handwritten digits as the training data -->
<!-- the database contains images of 28*28 pixels. so the first layer of our network will take each
of those pixels. and the output should give us 10 (one of the 10 digits) numbers, the maximum of
which we can call the prediction of the model -->
<!-- now because of the size of the training data required, this model would be infeasible to demo -->
<!-- as it takes too much time to train -->
<!-- and also, we need to write code to parse the database as well which turned out to be more
difficult than I had imagined -->
<!-- but anyway that's it from me. -->
# Larger demo
* use training data from MNIST database of handwritten digits
* `arch = {28*28, 16, 16, 10}`
* infeasible to demo
* too much time to train
* need to write code to parse the database

---
<!-- I know it's not much, but considering this is just a mini project and I am the only one doing
it. -->
<!-- so yeah, it ain't much but it's honest work -->
![w:900](./images/aint-much.png)
